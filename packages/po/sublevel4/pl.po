# Polish messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Copyright (C) 2004-2008 Bartosz Feński <fenio@debian.org>
#
# Marcin Owsiany <porridge@debian.org>, 2011, 2012.
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2016.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-10-08 12:04+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <debian-l10n-polish@lists.debian.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.5\n"

#. Type: text
#. Description
#. eg. Virtual disk 1 (xvda)
#. :sl4:
#: ../partman-base.templates:64001
#, no-c-format
msgid "Virtual disk %s (%s)"
msgstr "Wirtualny dysk %s (%s)"

#. Type: text
#. Description
#. eg. Virtual disk 1, partition #1 (xvda1)
#. :sl4:
#: ../partman-base.templates:65001
#, no-c-format
msgid "Virtual disk %s, partition #%s (%s)"
msgstr "Wirtualny dysk %s, partycja #%s (%s)"

#. Type: text
#. Description
#. :sl4:
#. Note to translators: Please keep your translations of this string below
#. a 65 columns limit (which means 65 characters in single-byte languages)
#: ../partman-basicfilesystems.templates:58001
msgid "acls - support POSIX.1e Access Control List"
msgstr "acls - obsługa list kontroli dostępu (ACL) POSIX.1e"

#. Type: text
#. Description
#. :sl4:
#. Note to translators: Please keep your translations of this string below
#. a 65 columns limit (which means 65 characters in single-byte languages)
#: ../partman-basicfilesystems.templates:59001
msgid "shortnames - only use the old MS-DOS 8.3 style filenames"
msgstr "krótkie nazwy - tylko krótkie nazwy MS-DOS 8.3"

#. Type: text
#. Description
#. :sl4:
#: ../partman-target.templates:3001
msgid ""
"In order to start your new system, a so called boot loader is used.  It is "
"installed in a boot partition.  You must set the bootable flag for the "
"partition.  Such a partition will be marked with \"${BOOTABLE}\" in the main "
"partitioning menu."
msgstr ""
"Do uruchamiania systemu, używany jest tak zwany program rozruchowy. Jest "
"instalowany na partycji rozruchowej. Trzeba dla niej ustawić flagę "
"rozruchową. Taka partycja będzie oznaczona jako \"${BOOTABLE}\" w głównym "
"menu partycjonowania."

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:1001
#, no-c-format
msgid "!! ERROR: %s"
msgstr "!! BŁĄD: %s"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:2001
msgid "KEYSTROKES:"
msgstr "SKRÓTY KLAWIATUROWE:"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:3001
#, no-c-format
msgid "'%c'"
msgstr "\"%c\""

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:4001
msgid "Display this help message"
msgstr "Wyświetla te informacje"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:5001
msgid "Go back to previous question"
msgstr "Powrót do poprzedniego pytania"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:6001
msgid "Select an empty entry"
msgstr "Wybierz pusty wpis"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:7001
#, no-c-format
msgid "Other choices are available with '%c' and '%c'"
msgstr "Dostępne są inne pola do wyboru (za pomocą \"%c\" i \"%c\")"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:8001
#, no-c-format
msgid "Previous choices are available with '%c'"
msgstr "Poprzednie pola do wyboru są dostępne za pomocą \"%c\""

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:9001
#, no-c-format
msgid "Next choices are available with '%c'"
msgstr "Następne pola do wyboru są dostępne za pomocą \"%c\""

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:12001
#, no-c-format
msgid "Prompt: '%c' for help, default=%d> "
msgstr "Wprowadź: '%c' by uzyskać pomoc, domyślnie=%d> "

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:13001
#, no-c-format
msgid "Prompt: '%c' for help> "
msgstr "Wpisz: '%c' by uzyskać pomoc> "

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:14001
#, no-c-format
msgid "Prompt: '%c' for help, default=%s> "
msgstr "Wprowadź: '%c' by uzyskać pomoc, domyślnie=%s> "

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:15001
msgid "[Press enter to continue]"
msgstr "[Wciśnij enter by kontynuować]"

#. Type: select
#. Description
#. :sl4:
#: ../cdebconf.templates:1001
msgid "Interface to use:"
msgstr "Interfejs do wykorzystania:"

#. Type: select
#. Description
#. :sl4:
#: ../cdebconf.templates:1001
msgid ""
"Packages that use debconf for configuration share a common look and feel. "
"You can select the type of user interface they use."
msgstr ""
"Pakiety korzystające do konfiguracji z debconfa współdzielą jeden wygląd i "
"sposób użycia. Można wybrać wykorzystywany do tego rodzaj interfejsu."

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:2001
msgid "None"
msgstr "Brak"

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:2001
msgid "'None' will never ask you any question."
msgstr "\"Brak\" spowoduje niezadawanie pytań użytkownikowi."

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:3001
msgid "Text"
msgstr "Tekst"

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:3001
msgid "'Text' is a traditional plain text interface."
msgstr "\"Tekst\" jest tradycyjnym interfejsem w zwykłym tekście."

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:4001
msgid "Newt"
msgstr "Newt"

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:4001
msgid "'Newt' is a full-screen, character based interface."
msgstr "\"Newt\" jest pełnoekranowym interfejsem znakowym."

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:5001
msgid "GTK"
msgstr "GTK"

#. Type: string
#. Description
#. :sl4:
#: ../cdebconf.templates:5001
msgid ""
"'GTK' is a graphical interface that may be used in any graphical environment."
msgstr ""
"\"GTK\" jest graficznym interfejsem, który może być użyty w dowolnym "
"środowisku graficznym."

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001 ../grub-installer.templates:32001
msgid "Failed to mount /target/proc"
msgstr "Montowanie /target/proc nie powiodło się"

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001 ../grub-installer.templates:32001
msgid "Mounting the proc file system on /target/proc failed."
msgstr "Zamontowanie systemu plików proc na /target/proc nie powiodło się."

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001 ../grub-installer.templates:32001
msgid "Warning: Your system may be unbootable!"
msgstr "Uwaga: Twój system może się nie uruchamiać!"

#. Type: note
#. Description
#. :sl4:
#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001 ../nobootloader.templates:4001
msgid "Setting firmware variables for automatic boot"
msgstr "Ustawianie zmiennych firmware do automatycznego uruchamiania"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001
msgid ""
"Some variables need to be set in the Genesi firmware in order for your "
"system to boot automatically.  At the end of the installation, the system "
"will reboot.  At the firmware prompt, set the following firmware variables "
"to enable auto-booting:"
msgstr ""
"By móc uruchamiać nowy system automatycznie, należy ustawić pewne zmienne w "
"Genesi firmware. Po zakończeniu instalacji, system zostanie ponownie "
"uruchomiony. Przy znaku zachęty firmware ustaw następujące zmienne by "
"uaktywnić automatyczne uruchamianie:"

#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: note
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: note
#. Description
#. :sl4:
#. Translators, the 4th string of this description has been dropped from
#. PO files. It contains firmware commands and should not be translated.
#: ../nobootloader.templates:3001 ../arcboot-installer.templates:5001
msgid ""
"You will only need to do this once.  Afterwards, enter the \"boot\" command "
"or reboot the system to proceed to your newly installed system."
msgstr ""
"Trzeba to wykonać tylko raz. Później wprowadź polecenie \"boot\" lub uruchom "
"ponownie komputer by przejść do nowo zainstalowanego systemu."

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001
msgid ""
"Alternatively, you will be able to boot the kernel manually by entering, at "
"the firmware prompt:"
msgstr "Alternatywnie możesz uruchomić jądro ręcznie wprowadzając:"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"Some variables need to be set in CFE in order for your system to boot "
"automatically. At the end of installation, the system will reboot. At the "
"firmware prompt, set the following variables to simplify booting:"
msgstr ""
"By móc uruchamiać nowy system automatycznie, należy ustawić pewne zmienne w "
"CFE. Po zakończeniu instalacji, system zostanie ponownie uruchomiony. Przy "
"znaku zachęty firmware ustaw następujące zmienne by uprościć automatyczne "
"uruchamianie:"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"You will only need to do this once. This enables you to just issue the "
"command \"boot_debian\" at the CFE prompt."
msgstr ""
"Należy wykonać to tylko raz. Wprowadzenie tych ustawień pozwoli uruchomić "
"system wprowadzając \"boot_debian\" przy znaku zachęty CFE."

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"If you prefer to auto-boot on every startup, you can set the following "
"variable in addition to the ones above:"
msgstr ""
"Jeśli wolisz by system uruchamiał się automatycznie, możesz oprócz "
"powyższych ustawić następującą zmienną:"

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:17001
msgid "Install GRUB?"
msgstr "Zainstalować GRUB?"

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:17001
msgid ""
"GRUB 2 is the next generation of GNU GRUB, the boot loader that is commonly "
"used on i386/amd64 PCs. It is now also available for ${ARCH}."
msgstr ""
"GRUB 2 to następna generacja GNU GRUB, programu rozruchowego popularnego na "
"komputerach i386/amd64. Jest teraz dostępny dla ${ARCH}."

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:17001
msgid ""
"It has interesting new features but is still experimental software for this "
"architecture. If you choose to install it, you should be prepared for "
"breakage, and have an idea on how to recover your system if it becomes "
"unbootable. You're advised not to try this in production environments."
msgstr ""
"Ma interesujące nowe funkcje, ale na tej architekturze nadal jest "
"oprogramowaniem eksperymentalnym. Jeśli zdecydujesz się na jego instalację, "
"przygotuj się na błędy i zorientuj się jak odzyskać system jeśli rozruch "
"zawiedzie. Nie zaleca się robić tego w środowiskach produkcyjnych."

#. Type: text
#. Description
#. :sl4:
#: ../arcboot-installer.templates:1001
msgid "Install the Arcboot boot loader on a hard disk"
msgstr "Zainstaluj program rozruchowy Arcboot na dysku twardym"

#. Type: string
#. Description
#. :sl4:
#: ../arcboot-installer.templates:2001
msgid "Disk for boot loader installation:"
msgstr "Dysk do instalacji programu rozruchowego:"

#. Type: string
#. Description
#. :sl4:
#: ../arcboot-installer.templates:2001
msgid ""
"Arcboot must be installed into the volume header of a disk with a SGI "
"disklabel. Usually the volume header of /dev/sda is used. Please give the "
"device name of the disk on which to put arcboot."
msgstr ""
"Arcboot musi być zainstalowany w nagłówku woluminu dysku z etykietą SGI. "
"Zazwyczaj używany jest nagłówek woluminu /dev/sda. Podaj nazwę urządzenia "
"dysku na którym na być umieszczony arcboot."

#. Type: note
#. Description
#. :sl4:
#: ../arcboot-installer.templates:3001
msgid "Arcboot configured to use a serial console"
msgstr "Arcboot został skonfigurowany do używania konsoli szeregowej"

#. Type: note
#. Description
#. :sl4:
#: ../arcboot-installer.templates:3001
msgid ""
"Arcboot is configured to use the serial port ${PORT} as the console. The "
"serial port speed is set to ${SPEED}."
msgstr ""
"Arcboot jest skonfigurowany do korzystania z portu szeregowego ${PORT} jako "
"konsoli. Prędkość portu szeregowego jest ustawiona na ${SPEED}."

#. Type: boolean
#. Description
#. :sl4:
#: ../arcboot-installer.templates:4001
msgid "Arcboot installation failed.  Continue anyway?"
msgstr "Nie udało się zainstalować Arcboot. Kontynuować mimo tego?"

#. Type: boolean
#. Description
#. :sl4:
#: ../arcboot-installer.templates:4001
msgid ""
"The arcboot package failed to install into /target/.  Installing Arcboot as "
"a boot loader is a required step.  The install problem might however be "
"unrelated to Arcboot, so continuing the installation may be possible."
msgstr ""
"Pakiet arcboot nie został zainstalowany w /target/. Instalacja Arcboot jako "
"programu rozruchowego jest wymagana. Problem z instalacją może nie być "
"związany z Arcboot i możliwe, że będzie on działał pomimo tego."

#. Type: note
#. Description
#. :sl4:
#. Translators, the 4th string of this description has been dropped from
#. PO files. It contains firmware commands and should not be translated.
#: ../arcboot-installer.templates:5001
msgid "Setting PROM variables for Arcboot"
msgstr "Ustawianie zmiennych PROM dla Arcboot"

#. Type: note
#. Description
#. :sl4:
#. Translators, the 4th string of this description has been dropped from
#. PO files. It contains firmware commands and should not be translated.
#: ../arcboot-installer.templates:5001
msgid ""
"If this is the first Linux installation on this machine, or if the hard "
"drives have been repartitioned, some variables need to be set in the PROM "
"before the system is able to boot normally."
msgstr ""
"Jeśli jest to pierwsza instalacja Linuksa na tym komputerze, lub jeśli dyski "
"twarde zostały przepartycjonowane, niektóre zmienne PROM-u muszą być "
"ustawione zanim system będzie mógł być uruchomiony normalnie."

#. Type: note
#. Description
#. :sl4:
#. Translators, the 4th string of this description has been dropped from
#. PO files. It contains firmware commands and should not be translated.
#. "Stop for Maintenance" should be left in English
#: ../arcboot-installer.templates:5001
msgid ""
"At the end of this installation stage, the system will reboot.  After this, "
"enter the command monitor from the \"Stop for Maintenance\" option, and "
"enter the following commands:"
msgstr ""
"Na końcu etapu instalacyjnego, system zostanie ponownie uruchomiony. "
"Następnie wprowadź polecenie monitor z opcji \"Stop for Maintenance\" oraz "
"następujące polecenia:"

#. Type: text
#. Description
#. :sl4:
#: ../partman-nbd.templates:1001
msgid "Configure the Network Block Device"
msgstr "Konfiguracja sieciowego urządzenia blokowego"

#. Type: select
#. Description
#. :sl4:
#: ../partman-nbd.templates:2001
msgid "NBD configuration action:"
msgstr "Działania konfiguracyjne NBD:"

#. Type: select
#. Description
#. :sl4:
#: ../partman-nbd.templates:2001
msgid "There are currently ${NUMBER} devices connected."
msgstr "Obecnie jest podłączonych ${NUMBER} urządzeń."

#. Type: string
#. Description
#. :sl4:
#: ../partman-nbd.templates:3001
msgid "Network Block Device server:"
msgstr "Serwer sieciowego urządzenia blokowego:"

#. Type: string
#. Description
#. :sl4:
#: ../partman-nbd.templates:3001
msgid ""
"Please enter the host name or the IP address of the system running nbd-"
"server."
msgstr ""
"Proszę wprowadzić nazwę hosta lub adres IP systemu na którym działa nbd-"
"server."

#. Type: string
#. Description
#. :sl4:
#: ../partman-nbd.templates:4001
msgid "Name for NBD export"
msgstr "Nazwa eksportu NBD"

#. Type: string
#. Description
#. :sl4:
#: ../partman-nbd.templates:4001
msgid ""
"Please enter the NBD export name needed to access nbd-server. The name "
"entered here should match an existing export on the server."
msgstr ""
"Proszę wprowadzić nazwę eksportu NBD potrzebną do uzyskania dostępu do nbd-"
"server. Wprowadzona tu nazwa powinna być zgodna z istniejącym eksportem na "
"serwerze."

#. Type: select
#. Description
#. :sl4:
#: ../partman-nbd.templates:5001
msgid "Network Block Device device node:"
msgstr "Węzeł urządzenia sieciowego urządzenia blokowego:"

#. Type: select
#. Description
#. :sl4:
#: ../partman-nbd.templates:5001
msgid ""
"Please select the NBD device node that you wish to connect or disconnect."
msgstr ""
"Proszę wybrać węzeł urządzenia NBD które chcesz podłączyć lub odłączyć."

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:6001
msgid "Failed to connect to the NBD server"
msgstr "Nie powiodło się podłączenie do serwera NBD"

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:6001
msgid ""
"Connecting to the nbd-server failed. Please ensure that the host and the "
"export name which you entered are correct, that the nbd-server process is "
"running on that host, that the network is configured correctly, and retry."
msgstr ""
"Podłączenie do serwera nbd nie udało się. Proszę sprawdzić czy podana nazwa "
"hosta lub nazwa eksportu są poprawne, czy proces nbd-server działa na tym "
"hoście, oraz czy sieć jest poprawnie skonfigurowana, i spróbować ponownie."

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:7001
msgid "No more Network Block Device nodes left"
msgstr "Brak węzłów sieciowego urządzenia blokowego (NBD)."

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:7001
msgid ""
"Either all available NBD device nodes are in use or something went wrong "
"with the detection of the device nodes."
msgstr ""
"Albo wszystkie dostępne węzły urządzeń NBD są w użyciu, albo coś poszło źle "
"z wykrywaniem węzłów urządzeń."

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:7001
msgid ""
"No more NBD device nodes can be configured until a configured one is "
"disconnected."
msgstr ""
"Nie można skonfigurować więcej węzłów urządzeń NBD do czasu odłączenia "
"skonfigurowanego węzła."

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:8001
msgid "No connected Network Block Device nodes were found"
msgstr ""
"Nie znaleziono połączonych węzłów sieciowego urządzenia blokowego (NBD)"

#. Type: error
#. Description
#. :sl4:
#: ../partman-nbd.templates:8001
msgid ""
"There are currently no Network Block Device nodes connected to any server. "
"As such, you can't disconnect any of them."
msgstr ""
"W obecnej chwili nie ma żadnych węzłów sieciowego urządzenia blokowego (NBD) "
"podłączonych do żadnego serwera. W tej sytuacji nie można odłączyć żadnego z "
"nich."

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinite form
#: ../partman-nbd.templates:9001
msgid "Connect a Network Block Device"
msgstr "Podłączenie sieciowego urządzenia blokowego (NBD)"

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinite form
#: ../partman-nbd.templates:10001
msgid "Disconnect a Network Block Device"
msgstr "Odłączenie sieciowego urządzenia blokowego (NBD)"

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinite form
#: ../partman-nbd.templates:11001
msgid "Finish and return to the partitioner"
msgstr "Zakończenie i powrót do partycjonowania"

#. Type: text
#. Description
#. This item is a progress bar heading when the system configures
#. some flashable memory used by many embedded devices
#. :sl4:
#: ../flash-kernel-installer.templates:1001
msgid "Configuring flash memory to boot the system"
msgstr "Konfigurowanie pamięci flash do uruchamiania systemu"

#. Type: text
#. Description
#. This item is a progress bar heading when an embedded device is
#. configured so it will boot from disk
#. :sl4:
#: ../flash-kernel-installer.templates:2001
msgid "Making the system bootable"
msgstr "Przygotowanie systemu do rozruchu"

#. Type: text
#. Description
#. This is "preparing the system" to flash the kernel and initrd
#. on a flashable memory
#. :sl4:
#: ../flash-kernel-installer.templates:3001
msgid "Preparing the system..."
msgstr "Przygotowywanie systemu..."

#. Type: text
#. Description
#. This is a progress bar showing up when the system
#. write the kernel to the flashable memory of the embedded device
#. :sl4:
#: ../flash-kernel-installer.templates:4001
msgid "Writing the kernel to flash memory..."
msgstr "Zapisywanie jądra na pamięci flash..."

#. Type: text
#. Description
#. This is a progress bar showing up when the system generates a
#. special boot image on disk for some embedded device so they
#. can boot.
#. :sl4:
#: ../flash-kernel-installer.templates:5001
msgid "Generating boot image on disk..."
msgstr "Tworzenie obrazu rozruchowego na dysku..."

#. Type: text
#. Description
#. Main menu item
#. This item is a menu entry for a step where the system configures
#. the flashable memory used by many embedded devices
#. (writing the kernel and initrd to it)
#. :sl4:
#: ../flash-kernel-installer.templates:6001
msgid "Make the system bootable"
msgstr "Zainstaluj jądro na dysku"
